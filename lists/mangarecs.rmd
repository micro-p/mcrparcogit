---
title: Manga Recommendations
geometry: margin=1cm
output: pdf_document
---


* **liar game**
* **denjin n**
* **coppelion**
* **candy & cigarettes**
* **blood lad**
* **3d kanojo: real girl**
* **uzumaki**
* **tesogare otame x amnesia**
* **shin kidou senki gundam wing**
* **jinrou game: crazy fox**
* **kuzu no honkai**
* **servamp**
* **mondaiji-tachi ga isekai kara kuru sou desu yo?**
* **hai to gensou no grimgar**
* **outbreak company: moeru shinryakusha**
* **zero no tsukaima**
* **garie**
* **gokukoku no brynhildr**
* **usokui**
* **my home hero**
* **machida-kun no sekai**
* **nekota no koto ga ki ni natte shikata ga nai** _shojo light_
* **do you like drinking alcohol with beautiful women**
* **jimi na kensei wa sore demo saikyou desu**
* **my status as an assassin obviously exceeds the brave's**
* **12 beast**
* **knights and magic**
* **nakutemo yokute taemanaku hikaru** _shojo_
* **orc ga okashite kurenai!**
* **maou gaukin no futekigousha**
* **kakushigoto**
* **scum's wish**
* **dog nigga**
* **chijyou 100kai**
* **yuuna and the haunted hot springs**
* **infinite stratos**
* **haganai i don't have many friends**
* **to love ru**
* **citrus**
* **saikyou densetsu kurosawa**
* **psd radio**
* **iya na kao sare nagara opantsu misete moraitai**
* **miro tights**
* **ningen card**
* **kemokko doubutsuen**
* **veritas**
* **pokemon adventures**
* **yakedo shoujo**
