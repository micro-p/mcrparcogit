#  __  __ _                      ____
# |  \/  (_) ___ _ __ ___       |  _ \
# | |\/| | |/ __| '__/ _ \ _____| |_) |
# | |  | | | (__| | | (_) |_____|  __/
# |_|  |_|_|\___|_|  \___/      |_|
#
# feel free to use just gimme credits xD
# wm independent hotkeys
#
# dmenu
super + @space
	dmenu_run

super + grave
	dmenu_run

Menu
	dmenu_run

shift + Menu
	~/.local/bin/dmenuconfig
# rofi
super + d
	rofi -show
# terminal emulator
super + Return
	alacritty

ctrl + alt + t
	alacritty

# program launcher
super + F12
	dmenu_run

# emoji picker
super + a
	rofimoji -c

# Rss Reader
super + n
	alacritty -e "newsboat"

# file manager -- lf
super + e
	alacritty -e "lf"

# file manager -- ranger
super + ctrl + e
	alacritty --class scratch --dimensions 90 50 -e "lf"

# clipmenu
super + ctrl + c
	clipmenu

# file manager --pcmanfm
super + shift + e
	pcmanfm

# browser
super + q
	brave --process-per-site

# make sxhkd reload its configuration files:
super + Escape
	pkill -USR1 -x sxhkd
# xkill
super + shift + Escape
	xkill

# switch windows locally
alt + Tab
	bspc node -f next.local

alt + shift + Tab
	bspc node -f prev.local
#
# bspwm hotkeys
#

# quit/restart bspwm
super + alt + {q,r}
	bspc {quit,wm -r}

# close and kill
super + {_,shift + }c
	bspc node -{k,c}

# Rotate the tree from the current nodes parent
super + control + {Right, Left}
	bspc node @parent --rotate {90,270}

# Show/Hide Polybar
# super + shift + p
# 	/home/micro-p/.local/bin/togglepolybar

# Screenshot save.clipboard
super + p
	flameshot full -p /home/micro-p/Pictures/screenshots/ -c

# Screenshot Gui
super + shift + p
	flameshot gui


# restart polybar
super + shift + alt + p
	killall polybar && ~/.local/bin/launch_polybar

# alternate between the tiled and monocle layout
super + m
	bspc desktop -l next

# send the newest marked node to the newest preselected node
super + y
	bspc node newest.marked.local -n newest.!automatic.local

# swap the current node and the biggest local node
super + g
	bspc node -s biggest.local

## pulse audio volume control
#XF86Audio{RaiseVolume,LowerVolume}
#   pulseeffects gui
super + u
	pulseeffects

# Mute
XF86AudioMute
	amixer set Master toggle


# raises volume
XF86AudioRaiseVolume
	amixer set Master 5%+

# lowers volume
XF86AudioLowerVolume
	amixer set Master 5%-

# next song
super + equal
	playerctl next

XF86AudioPlay
	playerctl next

XF86AudioPause
	playerctl next

# previous song
super + minus
	playerctl previous


# Mute
XF86AudioMute
	amixer -D pulse set Master 1+ toggle

# pulsemixer
super + End
	alacritty -e pulsemixer

# brightness control
XF86MonBrightness{Up,Down}
	xbacklight {+ 10,- 10}

# screenshot
Print
	flameshot full -p /home/micro-p/Pictures/screenshots/ -c

# gui screenshot
shift + Print
	flameshot gui

# Print
# 	scrot '/tmp/%F_%T_$wx$h.png' -e 'xclip -selection clipboard -target image/png -i $f'

#
# state/flags
#

# set the window state
super + {t,shift + t,f,shift + f}
	bspc node -t {tiled,pseudo_tiled,floating,fullscreen}

# set the node flags
super + ctrl + {m,x,y,z}
	bspc node -g {marked,locked,sticky,private}

#
# focus/swap
#

# focus the node in the given direction
super + {_,shift + }{h,j,k,l}
	bspc node -{f,s} {west,south,north,east}

# focus the node for the given path jump
# super + {p,b,comma,period}
# 	bspc node -f @{parent,brother,first,second}

# focus the next/previous node in the current desktop
#super + {_,shift + }c
#	bspc node -f {next,prev}.local

# focus the next/previous desktop in the current monitor
# super + bracket{left,right}
# 	bspc config pointer_follows_focus false; \
# 	bspc desktop -f {prev,next}.local; \
# 	bspc config pointer_follows_focus true

# next/prev song
super + bracket{left,right}
	playerctl {previous,next}

super + Prior
	playerctl next

super + Next
	playerctl previous


# pause/play song
super + backslash
	playerctl play-pause

super + Home
	playerctl play-pause
# focus the last node/desktop
super + {shift + ,_} Tab
	bspc config pointer_follows_focus false; \
	bspc {node,desktop} -f last; \
	bspc config pointer_follows_focus true

# focus the older or newer node in the focus history
super + {o,i}
	bspc wm -h off; \
	bspc node {older,newer} -f; \
	bspc wm -h on

# focus or send to the given desktop
super + {_,shift + }{1-9,0}
	bspc config pointer_follows_focus false; \
	bspc {desktop -f,node -d} '^{1-9,10}'; \
	bspc config pointer_follows_focus true

#
# preselect
#

# preselect the direction
super + ctrl + {h,j,k,l}
	bspc node -p {west,south,north,east}

# preselect the ratio
super + ctrl + {1-9}
	bspc node -o 0.{1-9}

# cancel the preselection for the focused node
super + ctrl + space
	bspc node -p cancel

# cancel the preselection for the focused desktop
super + ctrl + shift + space
	bspc query -N -d | xargs -I id -n 1 bspc node id -p cancel

#
# move/resize
#

# expand a window by moving one of its side outward
super + alt + {h,j,k,l}
	bspc node -z {left -20 0,bottom 0 20,top 0 -20,right 20 0}

# contract a window by moving one of its side inward
super + alt + shift + {h,j,k,l}
	bspc node -z {right -20 0,top 0 20,bottom 0 -20,left 20 0}

# move a floating window
super + {Left,Down,Up,Right}
	bspc node -v {-20 0,0 20,0 -20,20 0}

# floating terminal
super + ctrl + s
	alacritty --class scratch --position 1035 445 --dimensions 80 30

# visualizer
super + shift + v
	alacritty --class scratch --position 1390 820 --dimensions 205 38 --config-file=/home/micro-p/.config/alacritty/alacritty_small.yml -e vis & sleep 1s && bspc node -g sticky=on

# emacs
super + ctrl + v
	emacs

# scratchpad tiled
super + v
	alacritty -e nvim

# scratchpad
super + s
	alacritty --class scratch --position 1035 445 --dimensions 80 30 -e nvim

# cheat-sheet
super + shift + s
	alacritty --class cheat --dimensions 80 30 -e /home/micro-p/.local/bin/cheat

# hide Polybar
super + w
	polybar-msg cmd toggle
