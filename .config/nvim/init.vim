"  __  __ _                      ____
" |  \/  (_) ___ _ __ ___       |  _ \
" | |\/| | |/ __| '__/ _ \ _____| |_) |
" | |  | | | (__| | | (_) |_____|  __/
" |_|  |_|_|\___|_|  \___/      |_|

let mapleader =","

if ! filereadable(expand('~/.config/nvim/autoload/plug.vim'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ~/.config/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ~/.config/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

call plug#begin('~/.config/nvim/plugged')
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdtree'
Plug 'tsony-tsonev/nerdtree-git-plugin'
Plug 'ryanoasis/vim-devicons'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'junegunn/goyo.vim'
"Plug 'PotatoesMaster/i3-vim-syntax'
Plug 'jreybert/vimagit'
"Plug 'lukesmithxyz/vimling'
"Plug 'vimwiki/vimwiki'
Plug 'bling/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-commentary'
Plug 'morhetz/gruvbox'
Plug 'kovetskiy/sxhkd-vim'
Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' }
Plug 'jiangmiao/auto-pairs'
"Plug 'dylanaraps/wal.vim'
"Plug 'tomasiser/vim-code-dark'
"Plug 'gryf/wombat256grf'
Plug 'tomasr/molokai'
Plug 'sonph/onehalf', {'rtp': 'vim/'}
Plug 'liuchengxu/space-vim-dark'
Plug 'yggdroot/indentLine'
" Plug 'ycm-core/YouCompleteMe'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
"Plug 'metakirby5/codi.vim'
Plug 'ChristianChiarulli/codi.vim'
"Plug 'ThePrimeagen/vim-be-good', {'do': './install.sh'}
"Plug 'rainglow/vim'
"Plug 'flazz/vim-colorschemes'
"Plug 'rafi/awesome-vim-colorschemes'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
" Plug 'SirVer/ultisnips'
" Plug 'honza/vim-snippets'
" tabular plugin is used to format tables
Plug 'godlygeek/tabular'
" JSON front matter highlight plugin
Plug 'elzr/vim-json'
Plug 'plasticboy/vim-markdown'
" Plug 'fadein/vim-FIGlet'
Plug 'chrisbra/changesPlugin'
call plug#end()

set termguicolors
set ic
set smartcase
set nobackup
set bg=light
set go=a
set mouse=a
set nowrap
set hlsearch
set clipboard=unnamedplus
set pastetoggle=<F2>
" set spell
set tabstop=4
set shiftwidth=4
set expandtab
set cursorline
" set cursorcolumn


" set color theme to pywal
" colorscheme wal
" colorscheme github-contrast
" colorscheme codecourse-contrast
colorscheme space-vim-dark
" highlight cursorline ctermbg=Yellow cterm=bold guibg=#191919
" highlight cursorcolumn ctermbg=Yellow cterm=bold guibg=#191919

let g:codi#virtual_text_prefix = "❯ "
" you complete me config
let g:ycm_min_num_of_chars_for_completion = 1
let g:ycm_max_num_identifier_candidates = 60
let g:ycm_disable_for_files_larger_than_kb = 0
let g:ycm_filetype_blacklist = {}
let g:ycm_complete_in_strings = 1
let g:ycm_collect_identifiers_from_comments_and_strings = 1
let g:ycm_collect_identifiers_from_tags_files = 1

" coc config

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

let g:indentLine_leadingSpaceEnabled =1
" airline config
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='serene'
let g:airline_powerline_fonts = 1

" Some basics:
	nnoremap c "_c
	set nocompatible
	filetype plugin on
	syntax on
	set encoding=utf-8
	set number relativenumber
	filetype detect

" Enable autocompletion:
	set wildmode=longest,list,full

" Disables automatic commenting on newline:
	autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

"recompile dwmblocks on exit
    autocmd BufWritePost ~/.config/dwmblocks/config.h !cd ~/.config/dwmblocks/; sudo -S make install && { killall -q dwmblocks;setsid dwmblocks & }

" Goyo plugin makes text more readable when writing prose:
	map <leader>f :Goyo \| set bg=light \| set linebreak<CR>

" Spell-check set to <leader>o, 'o' for 'orthography':
	map <leader>o :setlocal spell! spelllang=en_us<CR>

" Splits open at the bottom and right, which is non-retarded, unlike vim defaults.
	set splitbelow splitright

" Nerd tree
	map <leader>n :NERDTreeFind<CR>
	autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" NERDTree Git Plugin config
	let g:NERDTreeShowIgnoredStatus = 1
	let g:NERDTreeGitStatusWithFlags = 1

" FZF
	map <leader>p :Files<CR>

" " vimling:
" 	nm <leader>d :call ToggleDeadKeys()<CR>
" 	imap <leader>d <esc>:call ToggleDeadKeys()<CR>a
" 	nm <leader>i :call ToggleIPA()<CR>
" 	imap <leader>i <esc>:call ToggleIPA()<CR>a
" 	nm <leader>q :call ToggleProse()<CR>

" Shortcutting split navigation, saving a keypress:
	map <C-h> <C-w>h
	map <C-j> <C-w>j
	map <C-k> <C-w>k
	map <C-l> <C-w>l

set shell=sh
" Check file in shellcheck:
	map <leader>s :!clear && shellcheck %<CR>

" Open my bibliography file in split
	map <leader>b :vsp<space>$BIB<CR>
	map <leader>r :vsp<space>$REFER<CR>

" Replace all is aliased to S.
	nnoremap S :%s//g<Left><Left>

" Compile document, be it groff/LaTeX/markdown/etc.
	map <leader>c :w! \| !compiler <c-r>%<CR>

"
" Open corresponding .pdf/.html or preview
	map <leader>l :!opout <c-r>%<CR><CR>


" Runs a script that cleans out tex build files whenever I close out of a .tex file.
	autocmd VimLeave *.tex !texclear %

" Ensure files are read as what I want:
	let g:vimwiki_ext2syntax = {'.Rmd': 'markdown', '.rmd': 'markdown','.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}
	map <leader>v :VimwikiIndex
	let g:vimwiki_list = [{'path': '~/vimwiki', 'syntax': 'markdown', 'ext': '.md'}]
	autocmd BufRead,BufNewFile /tmp/calcurse*,~/.calcurse/notes/* set filetype=markdown
	autocmd BufRead,BufNewFile *.ms,*.me,*.mom,*.man set filetype=groff
	autocmd BufRead,BufNewFile *.tex set filetype=tex

" Save file as sudo on files that require root permission
	cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

" Enable Goyo by default for mutt writting
	autocmd BufRead,BufNewFile /tmp/neomutt* let g:goyo_width=80
	autocmd BufRead,BufNewFile /tmp/neomutt* :Goyo | set bg=light
	autocmd BufRead,BufNewFile /tmp/neomutt* map ZZ :Goyo\|x!<CR>
	autocmd BufRead,BufNewFile /tmp/neomutt* map ZQ :Goyo\|q!<CR>

" Automatically deletes all trailing whitespace on save.
	autocmd BufWritePre * %s/\s\+$//e

" When shortcut files are updated, renew bash and ranger configs with new material:
	autocmd BufWritePost files,directories !shortcuts
" Run xrdb whenever Xdefaults or Xresources are updated.
	autocmd BufWritePost *Xresources,*Xdefaults !xrdb %
" Update binds when sxhkdrc is updated.
	autocmd BufWritePost *sxhkdrc !pkill -USR1 sxhkd

" Turns off highlighting on the bits of code that are changed, so the line that is changed is highlighted but the actual text that has changed stands out on the line and is readable.
if &diff
    highlight! link DiffText MatchParen
endif

" yank highlight
 augroup highlight_yank
     autocmd!
     autocmd TextYankPost * silent lua require'vim.highlight'.on_yank()
 augroup END


" disable header folding
let g:vim_markdown_folding_disabled = 1

" do not use conceal feature, the implementation is not so good
let g:vim_markdown_conceal = 0

" disable math tex conceal feature
let g:tex_conceal = ""
let g:vim_markdown_math = 1

" support front matter of various format
let g:vim_markdown_frontmatter = 1  " for YAML format
let g:vim_markdown_toml_frontmatter = 1  " for TOML format
let g:vim_markdown_json_frontmatter = 1  " for JSON format

" color tags :)
let g:Hexokinase_highlighters = ['backgroundfull']

" map turn on color tags
map <leader>z :HexokinaseTurnOn<CR>
map <leader>Z :HexokinaseToggle<CR>


hi Normal guibg=NONE ctermbg=NONE
hi SpellBad cterm=undercurl gui=undercurl ctermfg=160 guifg=#FF4040 guibg=None ctermbg=None
hi SpellCap cterm=underline ctermfg=110 ctermbg=25 gui=undercurl guifg=#87afd7 guibg=NONE guisp=Blue

"blink -  codecourse - crisp - downpour - earthsong - frantic - frontier - github - glance - goldfish - hawaii - heroku
"horizon - hub - newton - patriot - potpourri - snappy - stark - tron -
