# Basic vars
set shell bash
set info time
set shellopts '-eu'
set ifs "\n"
set scrolloff 10
# set color256 on
set icons
set period 1

# Vars that depend on environmental variables
$lf -remote "send $id set previewer ${XDG_CONFIG_HOME:-$HOME/.config}/lf/scope"

# cmds/functions

cmd open ${{
    case $(file --mime-type $f -b) in
	image/vnd.djvu|application/pdf|application/octet-stream) setsid -f zathura $fx >/dev/null 2>&1 ;;
        text/*) $EDITOR $fx;;
	image/x-xcf|image/svg+xml) setsid -f gimp $f >/dev/null 2>&1 ;;
	image/*) rotdir $f | grep -i "\.\(png\|jpg\|jpeg\|gif\|webp\|tif\)\(_large\)*$" | sxiv -aio 2>/dev/null | lf-select ;;
	audio/*) mpv --audio-display=no $f ;;
	video/*) setsid -f mpv $f -quiet >/dev/null 2>&1 ;;
	application/pdf|application/vnd*|application/epub*) setsid -f zathura $fx >/dev/null 2>&1 ;;
        *) for f in $fx; do setsid -f $OPENER $f >/dev/null 2>&1; done;;
    esac
}}

# cmd open ${{
#     case $(file --mime-type "$f" -bL) in
#         text/*|application/json) $EDITOR "$f";;
#         video/*|image/*/application/pdf) xdg-open "$f";;
#         *) xdg-open "$f" ;;
#     esac
# }}}

cmd mkdir $mkdir -p "$(echo $* | tr ' ' '\ ')"

cmd moveto ${{
	clear; tput cup $(($(tput lines)/3)); tput bold
	set -f
	clear; echo "Move to where?"
	dest="$(cut -d'	' -f2- ${XDG_CONFIG_HOME:-$HOME/.config}/directories | fzf | sed 's|~|$HOME|' )" &&
	for x in $fx; do
		eval mv -iv \"$x\" \"$dest\"
	done &&
	notify-send "🚚 File(s) moved." "File(s) moved to $dest."
}}

cmd copyto ${{
	clear; tput cup $(($(tput lines)/3)); tput bold
	set -f
	clear; echo "Copy to where?"
	dest="$(cut -d'	' -f2- ${XDG_CONFIG_HOME:-$HOME/.config}/directories | fzf | sed 's|~|$HOME|' )" &&
	for x in $fx; do
		eval cp -ivr \"$x\" \"$dest\"
	done &&
	notify-send "📋 File(s) copied." "File(s) copies to $dest."
}}

# Trash cli bindings
cmd trash ${{
  files=$(printf "$fx" | tr '\n' ';')
  while [ "$files" ]; do
    # extract the substring from start of string up to delimiter.
    # this is the first "element" of the string.
    file=${files%%;*}

    trash-put "$(basename "$file")"
    # if there's only one element left, set `files` to an empty string.
    # this causes us to exit this `while` loop.
    # else, we delete the first "element" of the string from files, and move onto the next.
    if [ "$files" = "$file" ]; then
      files=''
    else
      files="${files#*;}"
    fi
  done
}}

cmd clear_trash %trash-empty

cmd restore_trash ${{
  trash-restore
}}

cmd bulkrename ${{
    vidir
}}

cmd newfold ${{
    set -f
    read newd
    mkdir $newd
    mv $fx $newd
}}

cmd bulk-rename ${{
    old=$(mktemp)
    new=$(mktemp)
    [ -n $fs ] && fs=$(ls)
    printf "$fs\n" > $old
    printf "$fs\n" > $new
    $EDITOR $new
    [ $(cat $new | wc -l) -ne $(cat $old | wc -l) ] && exit
    paste $old $new | while read names; do
        src=$(printf $names | cut -f1)
        dst=$(printf $names | cut -f2)
        [ $src = $dst ] && continue
        [ -e $dst ] && continue
        mv $src $dst
    done
    rm $old $new
    lf -remote "send $id unselect"
}}

cmd dragon %dragon-drag-and-drop -a -x $fx
cmd dragon-stay %dragon-drag-and-drop -a $fx
cmd dragon-individual %dragon-drag-and-drop $fx
cmd cpdragon %cpdragon
cmd mvdragon %mvdragon
cmd dlfile %dlfile

# Bindings
# Remove some defaults
map d
map c
map u
map p
map y
map S push :glob-select<space>


map <c-c> clear
map uc clear
map uu unselect

map <c-f> $lf -remote "send $id select '$(fzf)'"
map J $lf -remote "send $id cd $(cut -d'	' -f2 ${XDG_CONFIG_HOME:-$HOME/.config}/directories | fzf)"
map gh
map gf $lf -remote "send $id select '$(fzf)'"
map gg top
map yy copy
map D delete
map C copyto
map M moveto
map pp paste
map <c-n> push :mkdir<space>
map <c-r> reload
map <enter> shell
map <lt> $$f
map <gt> !$f
map o &mimeopen $f
map O $mimeopen --ask $f
map cd push :cd<space>
map dd cut
map cn push :mkdir<space>
map <a-n> newfold

# Dragon Mapping
map <c-d> dragon
map dr dragon
map ds dragon-stay
map di dragon-individual
map dm mvdragon
map dc cpdragon
map dl dlfile

map . set hidden!

map ca rename # at the very end
map cr push A<c-u> # new rename
map ci push A<c-a> # at the very beginning
map cc push A<a-b><a-b><a-f> # before extention
map ce push A<a-b> # after extention
map cb bulk-rename
map r bulkrename
map R bulk-rename

map V push :!nvim<space>

# Trash Mappings
map xx trash
map tc clear_trash
map tr restore_trash

# Movement
map gtr cd ~/.local/share/Trash/files
map gm cd /run/media/micro-p
map gs cd /mnt/A81E8AC21E8A88D0/StX/psx
map gp cd /mnt/
map ~ cd ~

map gv. cd ~/Videos
map gvm cd ~/Videos/musicvideos
map gvM cd ~/Videos/movies
map gva cd ~/Videos/anime
map gvt cd ~/Videos/tv\ series

map gP. cd ~/Pictures
map gPs cd ~/Pictures/screenshots

map gd cd ~/Documents

map gD cd ~/Downloads

map gc cd ~/.config
