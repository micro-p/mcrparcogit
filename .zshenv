export ZSH="/home/micro-p/.oh-my-zsh"
export EDITOR=nvim
export EDITOR=nvim visudo
export VISUAL=nvim
export SXHKD_SHELL=/bin/zsh
export SHELL=/bin/zsh
export MANPAGER='nvim -R +":set ft=man" -'
export HISTCONTROL=ignoreboth:erasedups
export PATH=$PATH:/home/micro-p/.bin:/home/micro-p/.local/bin:/home/micro-p/.local/share:~/.npm.g
export QT_QPA_PLATFORMTHEME="qt5ct"

